// File:    GroupC.h
// Authors: Tyler Orr, Kevin Smith, Mary Lauren Harris

#ifndef GROUP_C_H
#define GROUP_C_H

#include <SDL/SDL.h>
#include <utility>
#include <vector>
#include <queue>
#include <set>
#include <iostream>

#include "api.h"

using namespace std;

#define INF         10000000000000.0
#define MAX_DEPTH   3               // Must be > 1

//#define MOBILITY_VAL 200.0
//#define CORNER_VAL   3000.0
//#define CLOSE_VAL    2000.0
//#define FRONTIER_VAL 500.0
//#define PARITY_VAL   40.0

#define PARITY_VAL   80.0
#define MOBILITY_VAL 200.0
#define CORNER_VAL   500.0
#define CLOSE_VAL    350.0
#define FRONTIER_VAL 100.0
#define EDGE_VAL     250.0

/*******************************************************************************
 * Utility Function Declarations
 ******************************************************************************/
vector<pair<int,int> > getFlips(Board& b, pair<int,int> move, int color);
vector<pair<int, int> > getFlips(Board& b, pair<int,int> move,
                                 const pair<int,int> dir, int color);
bool isInBounds(pair<int,int> p, Board& b);
pair<int,int> operator+(pair<int,int> a, pair<int,int> b);
pair<int,int> numPieces(Board& b);
int makeMove(Board& b, pair<int,int> move, int color);
void flip(Board& b, pair<int,int> p);
int swapColor(int);

/*******************************************************************************
 * Node
 ******************************************************************************/
struct Node {
    float value;
    int color;
    pair<int,int> move;
    vector<Node*> children;

    Node(pair<int,int> m, int c) {
        move = m;
        color = c;
    }

    bool operator<(const Node &n) {
        if(this->value < n.value)
            return true;
        return false;
    }
};

/*******************************************************************************
 * GroupC
 ******************************************************************************/
class GroupC: public Player {

private:
    Node* gameTree;
    int otherColor;             // Opponent's color
    set<pair<int, int> > possibleMoves;
    set<pair<int, int> > tempPossibleMoves;
    int movesSoFar;
    int boundary[4];

    void  remove(Node*, int save = -1);
    void  getBestMove(pair<int,int>&);
    float alphaBeta(Node*, int, float, float, int, Board&);
    float getBoardValue(Board&);
    void  initWeights(Board&);
    void  generateChildren(Node*, int, Board&);
    bool  isLegal(Board&, pair<int, int>, int);
    static bool sortByValue(Node* a, Node* b);

public:
    GroupC(int color = BLACK);
    virtual ~GroupC();
    virtual void move(Board b, pair<int,int>&);

};

#endif
