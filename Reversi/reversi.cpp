
#include "reversi.h"

/*******************************************************************************
 * MyPlayer::MyPlayer()
 ******************************************************************************/
MyPlayer::MyPlayer(int strgy, int color) : Player(color) {
    srand((int)clock());
    strategy = strgy;
}

/*******************************************************************************
 * MyPlayer::~MyPlayer()
 ******************************************************************************/
MyPlayer::~MyPlayer() {
    // Do nothing
}

/*******************************************************************************
 * MyPlayer::move()
 ******************************************************************************/
void MyPlayer::move(Board b, pair<int,int>& nextMove) {
    vector<pair<int, int> > flips;
    
    if(!strategy) { // Random move selection
        int time = 0;
        while(!flips.size() && time < b.getRow() * b.getCol()) {
            nextMove.first = rand() % b.getRow();
            nextMove.second = rand() % b.getCol();
            flips = getFlips(b, nextMove, getColor());
            time++;
        }
    }
    else if(strategy == 1) { // Maximum piece swap
        int max = 0;
        pair<int,int> tempMove(0,0);
        for(int r = 0; r < b.getRow(); r++) {
            for(int c = 0; c < b.getCol(); c++) {
                tempMove.first = r;
                tempMove.second = c;
                flips = getFlips(b, tempMove, getColor());
                if(flips.size() > max) {
                    max = (int)flips.size();
                    nextMove = tempMove;
                }
            }
        }
    }
    else if(strategy == 2) { // Max/Min piece swap
        pair<int,int> tempMove(0,0);
        pair<int,int> pieces = numPieces(b);
        if(pieces.first + pieces.second > 32 ||
           ((getColor() == WHITE) ? (pieces.first < 5) : (pieces.second < 5))) {
            int max = 0;
            for(int r = 0; r < b.getRow(); r++) {
                for(int c = 0; c < b.getCol(); c++) {
                    tempMove.first = r;
                    tempMove.second = c;
                    flips = getFlips(b, tempMove, getColor());
                    if(flips.size() > max) {
                        max = (int)flips.size();
                        nextMove = tempMove;
                    }
                }
            }
        }
        else {
            int min = b.getRow() * b.getCol();
            for(int r = 0; r < b.getRow(); r++) {
                for(int c = 0; c < b.getCol(); c++) {
                    tempMove.first = r;
                    tempMove.second = c;
                    flips = getFlips(b, tempMove, getColor());
                    if(flips.size() < min && flips.size() > 0) {
                        min = (int)flips.size();
                        nextMove = tempMove;
                    }
                }
            }
        }
    }
    else if(strategy == 3) { // Minimum piece swap
        pair<int,int> tempMove(0,0);
        int min = b.getRow() * b.getCol();
        for(int r = 0; r < b.getRow(); r++) {
            for(int c = 0; c < b.getCol(); c++) {
                tempMove.first = r;
                tempMove.second = c;
                flips = getFlips(b, tempMove, getColor());
                if(flips.size() < min && flips.size() > 0) {
                    min = (int)flips.size();
                    nextMove = tempMove;
                }
            }
        }
    }
}

/*******************************************************************************
 * HumanPlayer::HumanPlayer()
 ******************************************************************************/
HumanPlayer::HumanPlayer(int color) : Player(color) {
    // Do nothing
}

/*******************************************************************************
 * HumanPlayer::~HumanPlayer()
 ******************************************************************************/
HumanPlayer::~HumanPlayer() {
    // Do nothing
}

/*******************************************************************************
 * HumanPlayer::move()
 ******************************************************************************/
void HumanPlayer::move(Board b, pair<int,int>& nextMove) {
    SDL_Event event;
    pair<int,int> np = numPieces(b);
    while((nextMove.first < 0 || nextMove.second < 0) && np.first && np.second &&
          np.first + np.second < b.getRow() * b.getCol()) {
        
        SDL_PollEvent(&event);
        if( event.type == SDL_QUIT )
            exit(0);
        else if(event.type == SDL_MOUSEBUTTONDOWN &&
                event.button.button == SDL_BUTTON_LEFT) {
            nextMove.second = event.button.x / (FRAME_SIZE / b.getCol());
            nextMove.first = event.button.y / (FRAME_SIZE / b.getRow());
            if(!(getFlips(b, nextMove, getColor()).size()))
               nextMove.first = nextMove.second = -1;
        }
    }
}



