//
//#include "Util.h"
//
///*******************************************************************************
// * getFlips
// ******************************************************************************/
//vector<pair<int,int> > getFlips(Board& b, pair<int,int> move, int color) {
//
//    vector<pair<int,int> > sol;
//    vector<pair<int,int> > temp;
//
//    if(isInBounds(move, b) && b[move.first][move.second] == OPEN) {
//
//        temp = getFlips(b, move, NORTH_EAST, color);
//        sol.insert(sol.end(), temp.begin(), temp.end());
//        temp = getFlips(b, move, NORTH, color);
//        sol.insert(sol.end(), temp.begin(), temp.end());
//        temp = getFlips(b, move, NORTH_WEST, color);
//        sol.insert(sol.end(), temp.begin(), temp.end());
//        temp = getFlips(b, move, WEST, color);
//        sol.insert(sol.end(), temp.begin(), temp.end());
//        temp = getFlips(b, move, EAST, color);
//        sol.insert(sol.end(), temp.begin(), temp.end());
//        temp = getFlips(b, move, SOUTH_WEST, color);
//        sol.insert(sol.end(), temp.begin(), temp.end());
//        temp = getFlips(b, move, SOUTH, color);
//        sol.insert(sol.end(), temp.begin(), temp.end());
//        temp = getFlips(b, move, SOUTH_EAST, color);
//        sol.insert(sol.end(), temp.begin(), temp.end());
//    }
//
//    return sol;
//}
//
///*******************************************************************************
// * getFlips
// ******************************************************************************/
//vector<pair<int, int> > getFlips(Board& b, pair<int,int> move,
//                                 const pair<int,int> dir, int color) {
//    move = move + dir;
//
//    int tgtColor = swapColor(color);
//    vector<pair<int,int> > sol;
//    vector<pair<int,int> > empty;
//
//    while (isInBounds(move, b) && b[move.first][move.second] == tgtColor) {
//        sol.push_back(move);
//        move = move + dir;
//    }
//
//    if(isInBounds(move, b) && b[move.first][move.second] == color)
//        return sol;
//
//    return empty;
//}
//
///*******************************************************************************
// * isInBounds
// ******************************************************************************/
//bool isInBounds(pair<int,int> p, Board& b) {
//    return (p.first >= 0 && p.first < b.getRow() &&
//            p.second >= 0 && p.second < b.getCol());
//}
//
///*******************************************************************************
// * operator+
// ******************************************************************************/
//pair<int,int> operator+(pair<int,int> a, pair<int,int> b) {
//    pair<int,int> c;
//    c.first = a.first + b.first;
//    c.second = a.second + b.second;
//    return c;
//}
//
///*******************************************************************************
// * numPieces
// ******************************************************************************/
//pair<int,int> numPieces(Board b) {
//    pair<int,int> p(0,0);
//    for(int i = 0; i < b.getRow(); i++) {
//        for(int j = 0; j < b.getCol(); j++) {
//            if(b[i][j] == WHITE)
//                p.first++;
//            else if(b[i][j] == BLACK)
//                p.second++;
//        }
//    }
//    return p;
//}
//
///*******************************************************************************
// * makeMove
// ******************************************************************************/
//int makeMove(Board& b, pair<int,int> move, int color) {
//
//    vector<pair<int, int> > flips;
//
//    flips = getFlips(b, move, color);
//
//    if(flips.size()) {
//
//        b[move.first][move.second] = color;
//
//        for(int i = 0; i < flips.size(); i++)
//            flip(b, flips[i]);
//    }
//
//    return (flips.size()) ? 0 : 1;
//}
//
///*******************************************************************************
// * flip
// ******************************************************************************/
//void flip(Board& b, pair<int,int> p) {
//    if(b[p.first][p.second] != OPEN)
//        b[p.first][p.second] = swapColor(b[p.first][p.second]);
//}
//
///*******************************************************************************
// * tileCount
// ******************************************************************************/
//int tileCount(Board b, int color) {
//
//    int count = 0;
//
//    for(int i = 0; i < b.getRow(); i++) {
//        for(int j = 0; j < b.getCol(); j++) {
//            if(b[i][j] == color)
//                count++;
//        }
//    }
//
//    return count;
//}
//
//int swapColor(int color) {
//    if(color == BLACK) return WHITE;
//    if(color == WHITE) return BLACK;
//    return OPEN;
//}
