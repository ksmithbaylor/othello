//
//#ifndef UTIL_H
//#define UTIL_H
//
//#include <vector>
//
//#include "api.h"
//
//using namespace std;
//
//const pair<int,int> NORTH_EAST(-1,1);
//const pair<int,int> NORTH(-1,0);
//const pair<int,int> NORTH_WEST(-1,-1);
//const pair<int,int> WEST(0,-1);
//const pair<int,int> EAST(0,1);
//const pair<int,int> SOUTH_WEST(1,-1);
//const pair<int,int> SOUTH(1,0);
//const pair<int,int> SOUTH_EAST(1,1);
//
///*******************************************************************************
// * getFlips
// *
// * Description: This function returns a vector of positions of tile flips that
// *              result from a given move.
// *
// * Pre:         b       Board object
// *              move    pair<int,int> representing row and col position of move
// *              color   int denoting color of new tile
// *
// * Post:        N/A
// *
// * Return:      vector of pair<int,int> representing tiles that are flipped
// ******************************************************************************/
//vector<pair<int,int> > getFlips(Board& b, pair<int,int> move, int color);
//
///*******************************************************************************
// * getFlips
// *
// * Description: This function returns a vecotr of position of tile flips in a
// *              specified linear direction that result from a given move.
// *
// * Pre:         b       Board object
// *              move    pair<int,int> representing row and col position of move
// *              dir     const pair<int,int> dnoting unit vector of direction
// *              color   int denoting color of new tile
// *
// * Post:        N/A
// *
// * Return:      vector of pair<int,int> representing tiles that are flipped in
// *              the given direction
// ******************************************************************************/
//vector<pair<int, int> > getFlips(Board& b, pair<int,int> move,
//                                 const pair<int,int> dir, int color);
//
///*******************************************************************************
// * isInBounds
// *
// * Description: This function determines if a given (row,col) is within the grid
// *
// * Pre:         p       pair<int,int> representing row and col position
// *
// * Post:        N/A
// *
// * Return:      Returns true if p is within the boundaries of the Reversi board
// ******************************************************************************/
//bool isInBounds(pair<int,int> p, Board& b);
//
///*******************************************************************************
// * operator+
// *
// * Description: This function adds to pairs of ints in the following form:
// *                  c.first  = a.first  + b.first
// *                  c.second = a.second + b.second
// *
// * Pre:         a       pair<int,int>
// *              b       pair<int,int>
// *
// * Post:        N/A
// *
// * Return:      pair<int,int>
// ******************************************************************************/
//pair<int,int> operator+(pair<int,int> a, pair<int,int> b);
//
///*******************************************************************************
// * numPieces
// *
// * Description: This function returns a pair of ints. The first is the number of
// *              white pieces on the board, and the second is the number of black
// *              pieces on the board.
// *
// * Pre:         b       Board object
// *
// * Post:        N/A
// *
// * Return:      pair<int,int> (number of white pieces, number of black pieces)
// ******************************************************************************/
//pair<int,int> numPieces(Board b);
//
///*******************************************************************************
// * makeMove
// *
// * Description: This function makes a move for a given player and draws it to
// *              the screen.
// *
// * Pre:         b       Board object
// *
// * Post:        Board b is adjusted based on the move made by the player.
// *
// * Return:      Returns 1 if the player passed, 0 otherwise.
// ******************************************************************************/
//int makeMove(Board& b, pair<int,int> move, int color);
//
///*******************************************************************************
// * flip
// *
// * Description: This function flips a piece on the board at a given location
// *
// * Pre:         b   Board object
// *              p   pair<int, int> denoting location to flip. The location is
// *                  assumed to be within the board.
// *
// * Post:        Location p on b will be be flipped (WHITE <--> BLACK)
// *
// * Return:      N/A
// ******************************************************************************/
//void flip(Board& b, pair<int,int> p);
//
///*******************************************************************************
// * tileCount
// *
// * Description: This function returns the number of tiles of a given color
// *
// * Pre:         b       Board object
// *              color   int denoting color to be counted
// *
// * Post:        N/A
// *
// * Return:      returns an int number of tiles of the given color
// ******************************************************************************/
//int tileCount(Board b, int color);
//
//int swapColor(int);
//
//#endif
