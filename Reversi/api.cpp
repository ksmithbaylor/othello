
#include "api.h"

/*******************************************************************************
 * Board
 ******************************************************************************/

Board::Board() {
    row = BOARD_R;
    col = BOARD_C;
    grid = new int*[row];
    for(int i = 0; i < row; i++) {
        grid[i] = new int[col];
    }
    
    for(int i = 0; i < row; i++) {
        for(int j = 0; j < col; j++) {
            grid[i][j] = OPEN;
        }
    }
    
    grid[(row/2) - 1][(col/2) - 1] = grid[(row/2)][(col/2)] = WHITE;
    grid[(row/2) - 1][(col/2)] = grid[(row/2)][(col/2) - 1] = BLACK;
}

Board::~Board() {
    for(int i = 0; i < row; i++) {
        delete [] grid[i];
    }
    delete [] grid;
    grid = NULL;
    row = col = 0;
}

Board::Board(const Board &b) {
    row = b.row;
    col = b.col;
    
    grid = new int*[row];
    
    for (int i = 0; i < row; i++)
        grid[i] = new int[col];
    
    for (int r = 0; r < row; r++) {
        for (int c = 0; c < col; c++)
            grid[r][c] = b.grid[r][c];
    }
}

Board Board::operator=(const Board &b) {
    if (this != &b) {
    
        for (int i = 0; i < row; i++)
            delete [] grid[i];
        
        delete [] grid;
        
        row = b.row;
        col = b.col;
        grid = new int*[row];
        
        for (int i = 0; i < row; i++) 
            grid[i] = new int[col];
        
        for (int r = 0; r < row; r++) {
            for (int c = 0; c < col; c++) 
                grid[r][c] = b.grid[r][c];
        }
    }
    
    return *this;
}

int Board::getRow() {
    return row;
}

int Board::getCol() {
    return col;
}

int* Board::operator[](int n) {
    return grid[n];
}


/*******************************************************************************
 * Player
 ******************************************************************************/

Player::Player(int c) {
    if(c == WHITE || c == BLACK)
        color = c;
}

Player::~Player() {
    // Do nothing
}

int Player::getColor() {
    return color;
}

void Player::setColor(int c) {
    if(c == WHITE || c == BLACK)
        color = c;
}

void Player::move(Board b, pair<int,int>& move) {
    // Do nothing
}
