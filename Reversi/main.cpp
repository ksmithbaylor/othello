
#include <iostream>
#include <vector>
#include <SDL/SDL.h>
#include <cmath>

#include "api.h"
#include "reversi.h"
#include "GroupC.h"

using namespace std;

#define NUM_GAMES 5

/*******************************************************************************
 * Function Declarations
 ******************************************************************************/
void draw(Board b, SDL_Surface *screen);
int  makeMove(Board& b, Player *p, SDL_Surface *screen);

/*******************************************************************************
 * main
 ******************************************************************************/
int main(int argc, char **argv) {
    
    if(SDL_Init(SDL_INIT_EVERYTHING) == -1) {
        cout << "Failed to load SDL.\n";
        exit(-1);
    }
    
    SDL_Surface *screen = SDL_SetVideoMode(FRAME_SIZE, FRAME_SIZE, 32,
                                           SDL_SWSURFACE | SDL_DOUBLEBUF);
    
    if(!screen) {
        cout << "Failed to load screen.\n";
        exit(-1);
    }
    
    int whiteTotal = 0;
    int blackTotal = 0;
    
    for(int i = 0; i < NUM_GAMES; i++) {
        
        Player* p1 = new MyPlayer(0, BLACK);
//        Player* p1 = new HumanPlayer(BLACK);
//        Player* p1 = new  GroupC(BLACK);

        
//        Player* p2 = new MyPlayer(2, WHITE);
//        Player* p2 = new HumanPlayer(WHITE);
        Player* p2 = new GroupC(WHITE);
        
        int     passes = 0;
        Board   b;
        
        draw(b, screen);
        
        while(passes < 2) {
            passes = 0;
            passes += makeMove(b, p1, screen);
            if(passes)
                cout << "BLACK Passes" << endl;
            passes += makeMove(b, p2, screen);
            if(passes)
                cout << "WHITE might have passed" << endl;
        }
        
        cout << "Game Over. Press ENTER to continue." << endl;
        
        // Pauses at end of game to check final game state.
        // TODO: Some games end prematurely, which we need to fix...
        cin.get();
        
        cout << "GAME " << i << endl;
        pair<int,int> pieces = numPieces(b);
        cout << "\tWhite:\t" << pieces.first << endl;
        whiteTotal += pieces.first;
        cout << "\tBlack:\t" << pieces.second << endl;
        blackTotal += pieces.second;
    }
    
    cout << "White total:\t" << whiteTotal << endl;
    cout << "Black total:\t" << blackTotal << endl << endl;
    cout << "White average:\t" << whiteTotal / NUM_GAMES << endl;
    cout << "Black average:\t" << blackTotal / NUM_GAMES << endl;
    
    SDL_FreeSurface(screen);
    SDL_Quit();
    return 0;
}

/*******************************************************************************
 * draw
 *
 * Description: This function draws the given Board to the given SDL_Surface
 *
 * Pre:         b       Board object to be rendered
 *              screen  SDL_Surface to be drawn to
 *
 * Post:        screen is changed by adding the contents of b
 *
 * Return:      N/A
 ******************************************************************************/
void draw(Board b, SDL_Surface *screen) {
    
    // Rect to determine location on screen
    SDL_Rect r;
    r.w = r.h = round(FRAME_SIZE / b.getRow());
    
    for(int i = 0; i < b.getRow(); i++) {
        for(int j = 0; j < b.getCol(); j++) {
            
            r.y = i * r.h;
            r.x = j * r.w;
            
            if(b[i][j] == WHITE)
                SDL_FillRect(screen, &r, 0xFFFFFFFF);
            else if(b[i][j] == BLACK)
                SDL_FillRect(screen, &r, 0x000000FF);
            else
                SDL_FillRect(screen, &r, 0x404040FF);
        }
    }
    
    SDL_Flip(screen);
//    SDL_Delay(200);
}

/*******************************************************************************
 * makeMove
 *
 * Description: This function makes a move for a given player and draws it to
 *              the screen.
 *
 * Pre:         b       Board object
 *              p       Player object
 *              screen  SDL_Surface to be drawn to
 *
 * Post:        Board b is adjusted based on the move made by the player and the
 *              new Board is printed to the screen.
 *
 * Return:      Returns 1 if the player passed, 0 otherwise.
 ******************************************************************************/
int makeMove(Board& b, Player *p, SDL_Surface *screen) {
    
    vector<pair<int, int> > flips;
    pair<int, int>          move(-1,-1);
    int                     color = p->getColor();
    
    p->move(b, move);
    flips = getFlips(b, move, color);
    
    if(flips.size()) {
        
        b[move.first][move.second] = color;
        draw(b, screen);
        
        for(int i = 0; i < flips.size(); i++)
            flip(b, flips[i]);
        draw(b, screen);
    }
    
    return (flips.size()) ? 0 : 1;
}
