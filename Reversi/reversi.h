
#ifndef REVERSI_H
#define REVERSI_H

#include <SDL/SDL.h>
#include <utility>
#include <cstdlib>
#include <ctime>
#include <vector>

#include "api.h"

using namespace std;

/*******************************************************************************
 * Function Declarations
 ******************************************************************************/
vector<pair<int,int> > getFlips(Board& b, pair<int,int> move, int color);
vector<pair<int, int> > getFlips(Board& b, pair<int,int> move,
                                 const pair<int,int> dir, int color);
bool isInBounds(pair<int,int> p, Board& b);
pair<int,int> operator+(pair<int,int> a, pair<int,int> b);
pair<int,int> numPieces(Board& b);
int makeMove(Board& b, pair<int,int> move, int color);
void flip(Board& b, pair<int,int> p);
int swapColor(int);

/*******************************************************************************
 * Player
 ******************************************************************************/
class MyPlayer: public Player {
    
    private:
        int strategy;
    
    public:
        MyPlayer(int strgy = 0, int color = BLACK);
        virtual ~MyPlayer();
        virtual void move(Board b, pair<int,int>&);
};

/*******************************************************************************
 * HumanPlayer
 ******************************************************************************/
class HumanPlayer: public Player {
    
private:
    
public:
    HumanPlayer(int color = BLACK);
    virtual ~HumanPlayer();
    virtual void move(Board b, pair<int,int>&);
};

#endif
