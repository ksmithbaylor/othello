// File:    GroupC.cpp
// Authors: Tyler Orr, Kevin Smith, Mary Lauren Harris

#include "GroupC.h"

const pair<int,int> NORTH_EAST(-1,1);
const pair<int,int> NORTH(-1,0);
const pair<int,int> NORTH_WEST(-1,-1);
const pair<int,int> WEST(0,-1);
const pair<int,int> EAST(0,1);
const pair<int,int> SOUTH_WEST(1,-1);
const pair<int,int> SOUTH(1,0);
const pair<int,int> SOUTH_EAST(1,1);

/*******************************************************************************
 * GroupC:: GroupC()
 ******************************************************************************/
 GroupC:: GroupC(int color) : Player(color) {
    gameTree = NULL;
    otherColor = swapColor(getColor());
}

/*******************************************************************************
 * GroupC::~ GroupC()
 ******************************************************************************/
 GroupC::~ GroupC() {
    remove(gameTree);
    gameTree = NULL;
}

/*******************************************************************************
 * GroupC::move()
 ******************************************************************************/
void GroupC::move(Board b, pair<int,int>& nextMove) {
    static int r = b.getRow(), c = b.getCol();
    static float gameStage;
    // Initialize tree if first move
    if(!gameTree) {
        if (BOARD_C <= 16 || BOARD_R <= 16) gameTree = new Node(pair<int,int>(-1,-1), otherColor);

        int boundLeft       = ((b.getCol() - 1) / 2) - 2;
        int boundRight      = ((b.getCol() - 1) / 2) + 3;
        int boundTop        = ((b.getRow() - 1) / 2) - 2;
        int boundBottom     = ((b.getRow() - 1) / 2) + 3;

        for (int r = boundTop; r <= boundBottom; r++) {
            for (int c = boundLeft; c <= boundRight; c++) {
                pair<int, int> p(r, c);
                if (isInBounds(p, b) && b[r][c] == OPEN)
                    possibleMoves.insert(p);
            }
        }
        
        movesSoFar = 4;
        
    } else {
        if (BOARD_C <= 16 || BOARD_R <= 16) {
            remove(gameTree);
            gameTree = new Node(pair<int,int>(-1,-1), otherColor);
        }
        
        movesSoFar += 2;
        gameStage = (r * c) - movesSoFar;

        int count = 0;
        
        for (int i = 0; i < 2 && count < 2; i++) {

            vector<pair<int,int> > toErase, toInsert;

            set<pair<int, int> >:: iterator itr = possibleMoves.begin();
            while (itr != possibleMoves.end() && count < 2) {
                if (b[itr->first][itr->second] != OPEN) {
                    toErase.push_back(*itr);
                    count++;

                    for (int r = itr->first - 1; r <= itr->first + 1; r++) {
                        for (int c = itr->second - 1; c <= itr->second + 1; c++) {
                            pair<int, int> p(r, c);
                            if (isInBounds(p, b) && b[r][c] == OPEN)
                                toInsert.push_back(p);
                        }
                    }
                }
                itr++;
            }

            for(int i = 0; i < toErase.size(); i++) {
                possibleMoves.erase(toErase[i]);
            }
            for(int i = 0; i < toInsert.size(); i++) {
                possibleMoves.insert(toInsert[i]);
            }
        }
    }
    
    
    if (BOARD_C <= 16 || BOARD_R <= 16 || gameStage > .75) {
        tempPossibleMoves = possibleMoves;
        
        // Solve using Iterative Deepening DFS
        for(int i = 1; i < MAX_DEPTH; i++) {
            sort(gameTree->children.rbegin(), gameTree->children.rend());
            float alpha = -INF, beta = INF;
            alphaBeta(gameTree, i, alpha, beta, getColor(), b);
            getBestMove(nextMove);
        }
    } else {
        int max = 0;
        vector<pair<int, int> > flips;
        pair<int,int> tempMove(0,0);
        for(int r = 0; r < b.getRow(); r++) {
            for(int c = 0; c < b.getCol(); c++) {
                tempMove.first = r;
                tempMove.second = c;
                flips = getFlips(b, tempMove, getColor());
                if(flips.size() > max) {
                    max = (int)flips.size();
                    nextMove = tempMove;
                }
            }
        }
    }
}

/*******************************************************************************
 * GroupC::remove()
 ******************************************************************************/
void GroupC::remove(Node* p, int save) {
    if(p) {
        for(int i = 0; i < p->children.size(); i++) {
            if(i != save) {
                remove(p->children[i]);
                p->children[i] = NULL;
            }
        }
        delete p;
    }
}

/*******************************************************************************
 * GroupC::getBestMove()
 ******************************************************************************/
void GroupC::getBestMove(pair<int,int>& nextMove) {
    if(!gameTree->children.empty()) {
        int max = gameTree->children[0]->value;
        nextMove = gameTree->children[0]->move;

        for(int i = 1; i < gameTree->children.size(); i++) {
            if(gameTree->children[i]->value > max) {
                max = gameTree->children[i]->value;
                nextMove = gameTree->children[i]->move;
            }
        }
    }
}

/*******************************************************************************
 * GroupC::alphaBeta()
 ******************************************************************************/
float GroupC::alphaBeta(Node* currNode, int depth, float a, float b, int color, Board& board) {
    static int r = board.getRow(), c = board.getCol();
    
    // Base Case
    if(depth <= 0)
        return currNode->value;

    // Generate children if necessary
    if(currNode->children.empty()) {
        generateChildren(currNode, color, board);
    }
    
    // We forced a pass
    if(currNode->children.empty()) {
        generateChildren(currNode, swapColor(color), board);
    }
    
    // The game ended
    if(currNode->children.empty()) {
        
        // WINNER FOUND!
        pair<int,int> pieces = numPieces(board);
        if(getColor() == WHITE) {
            if(pieces.first > pieces.second) {
                if(color == WHITE) return INF;
                else return -INF;
            }
            else if(pieces.first < pieces.second) {
                if(color == WHITE) return -INF;
                else return INF;
            }
        }
        else {
            if(pieces.first < pieces.second) {
                if(color == BLACK) return INF;
                else return -INF;
            }
            else if(pieces.first < pieces.second) {
                if(color == BLACK) return -INF;
                else return INF;
            }
        }
        
        return 0;
    }
    
    vector<pair<int, int> > flipped = getFlips(board, currNode->move, currNode->color);
    makeMove(board, currNode->move, currNode->color);
    
    vector<pair<int, int> > addedPossibleMoves;
    for (int r = currNode->move.first - 1; r <= currNode->move.first + 1; r++) {
        for (int c = currNode->move.second - 1; c <= currNode->move.second + 1; c++) {
            pair<int, int> p(r, c);
            if (isInBounds(p, board) && (board)[r][c] == OPEN) {
                if (tempPossibleMoves.find(p) == tempPossibleMoves.end()) {
                    tempPossibleMoves.insert(p);
                    addedPossibleMoves.push_back(p);
                }
            }
        }
    }
    tempPossibleMoves.erase(currNode->move);
    
    int recColor = swapColor(currNode->children[0]->color);
    
    float gameStage = (r * c) - movesSoFar;

    if(color == this->getColor()) {
        sort(currNode->children.rbegin(), currNode->children.rend(), sortByValue);
        for(int i = 0; i < currNode->children.size() && a < b; i++) {
            a = max(a, alphaBeta(currNode->children[i], depth - 1, a, b, recColor, board));
        }
        
        for(int i = 0; i < flipped.size(); i++) {
            flip(board, flipped[i]);
        }
        
        for (int i = 0; i < addedPossibleMoves.size(); i++) {
            tempPossibleMoves.erase(addedPossibleMoves[i]);
        }
        tempPossibleMoves.insert(currNode->move);
        
        if (gameStage < 0.15)      a += MOBILITY_VAL * currNode->children.size() * 1000;
        else if (gameStage > 0.7)  a += MOBILITY_VAL * currNode->children.size() * 0.2;
        else                       a += MOBILITY_VAL * currNode->children.size() * 1000;
    
        currNode->value = a;
        return a;
    }
    else {
        sort(currNode->children.begin(), currNode->children.end(), sortByValue);
        for(int i = 0; i < currNode->children.size() && a < b; i++) {
            b = min(b, alphaBeta(currNode->children[i], depth - 1, a, b, recColor, board));
        }
        
        for(int i = 0; i < flipped.size(); i++) {
            flip(board, flipped[i]);
        }
        
        for (int i = 0; i < addedPossibleMoves.size(); i++) {
            tempPossibleMoves.erase(addedPossibleMoves[i]);
        }
        tempPossibleMoves.insert(currNode->move);
        
        if (gameStage < 0.15)      a += MOBILITY_VAL * currNode->children.size() * 1000;
        else if (gameStage > 0.7)  a += MOBILITY_VAL * currNode->children.size() * 0.2;
        else                       a += MOBILITY_VAL * currNode->children.size() * 1000;
        
        currNode->value = b;
        return b;
    }
}

/*******************************************************************************
 * GroupC::getBoardValue()
 ******************************************************************************/
float GroupC::getBoardValue(Board& b) {
    static int r = b.getRow(), c = b.getCol();
    float gameStage = (r * c) - movesSoFar;
    static int ourColor = this->getColor();
    float corners, close, frontier, parity, mobility, edges;
    int ours, theirs, tempr, tempc;
    set<pair<int, int> > ourFront, theirFront;
    
    pair<int,int> pieces = numPieces(b);
    if(ourColor == WHITE)
       parity = 100.0 * (pieces.first - pieces.second) / (pieces.first + pieces.second);
    else
       parity = 100.0 * (pieces.second - pieces.first) / (pieces.first + pieces.second);

    // Find corner occupancy
    ours = theirs = 0;
    if (b[0][0] == ourColor) ours++;
    else if (b[0][0] == otherColor) theirs++;
    if (b[0][c-1] == ourColor) ours++;
    else if (b[0][c-1] == otherColor) theirs++;
    if (b[r-1][0] == ourColor) ours++;
    else if (b[r-1][0] == otherColor) theirs++;
    if (b[r-1][c-1] == ourColor) ours++;
    else if (b[r-1][c-1] == otherColor) theirs++;

    corners = 25.0 * (ours - theirs);

    // Find close to corners
    ours = theirs = 0;
    if (b[0][0] == OPEN) {
        if      (b[0][1] == ourColor) ours++;
        else if (b[0][1] == otherColor) theirs++;
        if      (b[1][1] == ourColor) ours++;
        else if (b[1][1] == otherColor) theirs++;
        if      (b[1][0] == ourColor) ours++;
        else if (b[1][0] == otherColor) theirs++;
    }
    if (b[0][c-1] == OPEN) {
        if      (b[0][c-2] == ourColor) ours++;
        else if (b[0][c-2] == otherColor) theirs++;
        if      (b[1][c-2] == ourColor) ours++;
        else if (b[1][c-2] == otherColor) theirs++;
        if      (b[1][c-1] == ourColor) ours++;
        else if (b[1][c-1] == otherColor) theirs++;
    }
    if (b[r-1][0] == OPEN) {
        if      (b[r-2][0] == ourColor) ours++;
        else if (b[r-2][0] == otherColor) theirs++;
        if      (b[r-2][1] == ourColor) ours++;
        else if (b[r-2][1] == otherColor) theirs++;
        if      (b[r-1][1] == ourColor) ours++;
        else if (b[r-1][1] == otherColor) theirs++;
    }
    if (b[r-1][c-1] == OPEN) {
        if      (b[r-2][c-1] == ourColor) ours++;
        else if (b[r-2][c-1] == otherColor) theirs++;
        if      (b[r-2][c-2] == ourColor) ours++;
        else if (b[r-2][c-2] == otherColor) theirs++;
        if      (b[r-1][c-2] == ourColor) ours++;
        else if (b[r-1][c-2] == otherColor) theirs++;
    }

    close = -12.0 * (ours - theirs);
    
    // Find edges
    ours = theirs = 0;
    
    for (int i = 2; i < r-2; i++) {
        if (b[0][i] == ourColor) ours++;
        else if (b[0][i] == otherColor) theirs++;
        if (b[i][0] == ourColor) ours++;
        else if (b[i][0] == otherColor) theirs++;
        if (b[r-1][i] == ourColor) ours++;
        else if (b[r-1][i] == otherColor) theirs++;
        if (b[i][c-1] == ourColor) ours++;
        else if (b[i][c-1] == otherColor) theirs++;
    }
    
    edges = 12.5 * (ours - theirs);
    
    // Find frontier
    ours = theirs = 0;
    set<pair<int, int> >:: iterator itr = tempPossibleMoves.begin();
    while (itr != tempPossibleMoves.end()) {
        if (b[itr->first][itr->second] == OPEN) {
//            if (isLegal(b, make_pair(itr->first, itr->second), ourColor)) ours++;
//            if (isLegal(b, make_pair(itr->first, itr->second), otherColor)) theirs++;
            for (int k = -1; k <= 1; k++) {
                for (int l = -1; l <= 1; l++) {
                    if (k || l) {
                        tempr = itr->first + k;
                        tempc = itr->second + l;
                        if (tempr >= 0 && tempr < r && tempc >= 0 && tempc < c) {
                            if (b[tempr][tempc] == ourColor) ourFront.insert(make_pair(itr->first, itr->second));
                            if (b[tempr][tempc] == otherColor) theirFront.insert(make_pair(itr->first, itr->second));
                        }
                    }
                }
            }
        }
        
        itr++;
    }
    
    if(ours + theirs) mobility = 100.0 * (ours / (ours + theirs));
    else mobility = 0;
    if (theirs > ours) mobility *= -1;

    if (ourFront.size() + theirFront.size()) frontier = 100.0 * (ourFront.size() / (ourFront.size() + theirFront.size()));
    else frontier = 0;
    if (theirFront > ourFront) frontier *= -1;
    
    // Opening game
    if (gameStage < 0.15) {
        return (corners  * CORNER_VAL    +
                close    * CLOSE_VAL     +
                frontier * FRONTIER_VAL  +
                parity   * PARITY_VAL   * 0.4  +
                mobility * MOBILITY_VAL * 1000 +
                edges    * EDGE_VAL );
    }
    
    // End game
    else if (gameStage > 0.7) {
        return (corners  * CORNER_VAL   * 1000  +
                close    * CLOSE_VAL    * 800 +
                frontier * FRONTIER_VAL * 0.2 +
                parity   * PARITY_VAL   * 1000 +
                mobility * MOBILITY_VAL * 0.2 +
                edges    * EDGE_VAL     * 80);
    }
    
    // Mid game
    else {
        return (corners  * CORNER_VAL   * 1000 +
                close    * CLOSE_VAL    * 800  +
                frontier * FRONTIER_VAL * 0.2  +
                parity   * PARITY_VAL   * 0.2 +
                mobility * MOBILITY_VAL * 1000 +
                edges    * EDGE_VAL     * 100);
    }
}
/*******************************************************************************
 * GroupC::generateChildren()
 ******************************************************************************/
void GroupC::generateChildren(Node* currNode, int color, Board& board) {
    
    pair<int,int> tempMove(-1,-1);

    set<pair<int, int> >:: iterator itr = tempPossibleMoves.begin();
    while (itr != tempPossibleMoves.end()) {

        tempMove.first = itr->first;
        tempMove.second = itr->second;

        if(isLegal(board, tempMove, color)) {
            Node* temp = new Node(tempMove, color);
            
//            vector<pair<int, int> > flipped = getFlips(board, temp->move, temp->color);
//            makeMove(board, temp->move, temp->color);
            
            temp->value = getBoardValue(board);
            
//            for(int i = 0; i < flipped.size(); i++) {
//                flip(board, flipped[i]);
//            }
//            board[tempMove.first][tempMove.second] = OPEN;

            currNode->children.push_back(temp);
        }

        itr++;
    }

    sort(currNode->children.begin(), currNode->children.end());
}

/*******************************************************************************
 * getFlips
 ******************************************************************************/
vector<pair<int,int> > getFlips(Board& b, pair<int,int> move, int color) {

    vector<pair<int,int> > sol;
    vector<pair<int,int> > temp;

    if(isInBounds(move, b) && b[move.first][move.second] == OPEN) {

        temp = getFlips(b, move, NORTH_EAST, color);
        sol.insert(sol.end(), temp.begin(), temp.end());
        temp = getFlips(b, move, NORTH, color);
        sol.insert(sol.end(), temp.begin(), temp.end());
        temp = getFlips(b, move, NORTH_WEST, color);
        sol.insert(sol.end(), temp.begin(), temp.end());
        temp = getFlips(b, move, WEST, color);
        sol.insert(sol.end(), temp.begin(), temp.end());
        temp = getFlips(b, move, EAST, color);
        sol.insert(sol.end(), temp.begin(), temp.end());
        temp = getFlips(b, move, SOUTH_WEST, color);
        sol.insert(sol.end(), temp.begin(), temp.end());
        temp = getFlips(b, move, SOUTH, color);
        sol.insert(sol.end(), temp.begin(), temp.end());
        temp = getFlips(b, move, SOUTH_EAST, color);
        sol.insert(sol.end(), temp.begin(), temp.end());
    }

    return sol;
}

/*******************************************************************************
 * getFlips
 ******************************************************************************/
vector<pair<int, int> > getFlips(Board& b, pair<int,int> move,
                                 const pair<int,int> dir, int color) {
    move = move + dir;

    int tgtColor = swapColor(color);
    vector<pair<int,int> > sol;
    vector<pair<int,int> > empty;

    while (isInBounds(move, b) && b[move.first][move.second] == tgtColor) {
        sol.push_back(move);
        move = move + dir;
    }

    if(isInBounds(move, b) && b[move.first][move.second] == color)
        return sol;

    return empty;
}

/*******************************************************************************
 * isInBounds
 ******************************************************************************/
bool isInBounds(pair<int,int> p, Board& b) {
    return (p.first >= 0 && p.first < b.getRow() &&
            p.second >= 0 && p.second < b.getCol());
}

/*******************************************************************************
 * operator+
 ******************************************************************************/
pair<int,int> operator+(pair<int,int> a, pair<int,int> b) {
    pair<int,int> c;
    c.first = a.first + b.first;
    c.second = a.second + b.second;
    return c;
}

/*******************************************************************************
 * numPieces
 ******************************************************************************/
pair<int,int> numPieces(Board& b) {
    pair<int,int> p(0,0);
    for(int i = 0; i < b.getRow(); i++) {
        for(int j = 0; j < b.getCol(); j++) {
            if(b[i][j] == WHITE)
                p.first++;
            else if(b[i][j] == BLACK)
                p.second++;
        }
    }
    return p;
}

/*******************************************************************************
 * makeMove
 ******************************************************************************/
int makeMove(Board& b, pair<int,int> move, int color) {

    vector<pair<int, int> > flips;

    flips = getFlips(b, move, color);

    if(flips.size()) {

        b[move.first][move.second] = color;

        for(int i = 0; i < flips.size(); i++)
            flip(b, flips[i]);
    }

    return (flips.size()) ? 0 : 1;
}

/*******************************************************************************
 * flip
 ******************************************************************************/
void flip(Board& b, pair<int,int> p) {
    if(b[p.first][p.second] != OPEN)
        b[p.first][p.second] = swapColor(b[p.first][p.second]);
}

/*******************************************************************************
 * swapColor
 ******************************************************************************/
int swapColor(int color) {
    if(color == BLACK) return WHITE;
    if(color == WHITE) return BLACK;
    return OPEN;
}

/*******************************************************************************
 * GroupC::isLegal
 ******************************************************************************/
bool GroupC::isLegal(Board& b, pair<int, int> move, int color) {
    int r = move.first, c = move.second;
    if (b[r][c] != OPEN) return false;

    int tempc, tempr, flipped, spot;
    int h = b.getRow(), w = b.getCol();
    bool keepGoing;

    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dx || dy) {
                tempc = move.second,
                tempr = move.first;
                flipped = 0;
                keepGoing = true;
                while (keepGoing) {
                    tempc += dx;
                    tempr += dy;

                    // in bounds
                    if (tempc >= 0 && tempc < w && tempr >= 0 && tempr < h) {
                        spot = b[tempr][tempc];
                        if (spot == OPEN) keepGoing = false;
                        else if (spot != color) {
                            flipped++;
                        } else {
                            if (!flipped) keepGoing = false;
                            else return true;
                        }
                    } else keepGoing = false;
                }
            }
        }
    }
    return false;
}

bool GroupC::sortByValue(Node* a, Node* b) {
    return a->value < b->value;
}

