
#ifndef API_H
#define API_H

#include <utility>

using namespace std;

#define BOARD_R 8
#define BOARD_C 8
#define FRAME_SIZE 800

const int OPEN  = 0;
const int BLACK = 1;
const int WHITE = 2;

/*******************************************************************************
 * Board
 ******************************************************************************/
class Board {
    
    private:
        int row, col;
        int **grid;
    
    public:
        Board();
        ~Board();
        Board(const Board &b);
        Board operator=(const Board &b);
        int getRow();
        int getCol();
        int* operator[](int n);
};

/*******************************************************************************
 * Player
 ******************************************************************************/
class Player {
    
	private:
		int color;

	public:
		Player(int c = BLACK);
		virtual ~Player();
		int getColor();
		void setColor(int c);
		virtual void move(Board, pair<int,int>&);
};

#endif
